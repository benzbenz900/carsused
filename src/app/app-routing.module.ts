import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './page/home/home.component';
import { SaleloginComponent } from './page/salelogin/salelogin.component';
import { StoreloginComponent } from './page/store/storelogin/storelogin.component';
import { SaleregisterComponent } from './page/saleregister/saleregister.component';
import { StoreregisterComponent } from './page/store/storeregister/storeregister.component';
import { StorehomeComponent } from './page/store/storehome/storehome.component';
import { HomepageComponent } from './page/index/homepage/homepage.component';
import { StorehomepageComponent } from './page/store/storehomepage/storehomepage.component';
import { StoreproductComponent } from './page/store/storeproduct/storeproduct.component';
import { AddcarnewComponent } from './page/store/addcarnew/addcarnew.component';
import { StorehowtoComponent } from './page/store/storehowto/storehowto.component';
const routes: Routes = [
  {
    path: '', component: HomepageComponent, children: [
      { path: '', component: HomeComponent },
      { path: 'index.html', component: HomeComponent },
      { path: 'salelogin.html', component: SaleloginComponent },
      { path: 'storelogin.html', component: StoreloginComponent },
      { path: 'saleregister.html', component: SaleregisterComponent },
      { path: 'storeregister.html', component: StoreregisterComponent },
    ]
  },
  {
    path: 'store', component: StorehomepageComponent, children: [
      {
        path: ':storeurl', children: [
          {
            path: '', children: [
              { path: '', component: StoreproductComponent },
              { path: 'profile', component: StorehomeComponent },
              { path: 'addnewcar', component: AddcarnewComponent },
              { path: 'howto', component: StorehowtoComponent },
              { path: 'contact', component: StorehowtoComponent },
              { path: ':status', component: StoreproductComponent },
            ]
          },
        ]
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
