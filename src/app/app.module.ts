import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Api } from './service/api'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HeaderComponent } from './section/header/header.component';
import { FooterComponent } from './section/footer/footer.component';
import { HomeComponent } from './page/home/home.component';
import { SaleloginComponent } from './page/salelogin/salelogin.component';
import { StoreloginComponent } from './page/store/storelogin/storelogin.component';
import { SaleregisterComponent } from './page/saleregister/saleregister.component';
import { StoreregisterComponent } from './page/store/storeregister/storeregister.component';
import { StorehomeComponent } from './page/store/storehome/storehome.component';
import { HomepageComponent } from './page/index/homepage/homepage.component';
import { StorehomepageComponent } from './page/store/storehomepage/storehomepage.component';
import { HeaderstoreComponent } from './section/headerstore/headerstore.component';
import { FooterstoreComponent } from './section/footerstore/footerstore.component';
import { StoreproductComponent } from './page/store/storeproduct/storeproduct.component';
import { AddcarnewComponent } from './page/store/addcarnew/addcarnew.component';
import { CKEditorModule } from 'ckeditor4-angular';
import { AgoPipe } from './pipe/ago.pipe';
import { StorehowtoComponent } from './page/store/storehowto/storehowto.component';
import {NgxPaginationModule} from 'ngx-pagination';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    SaleloginComponent,
    StoreloginComponent,
    SaleregisterComponent,
    StoreregisterComponent,
    StorehomeComponent,
    HomepageComponent,
    StorehomepageComponent,
    HeaderstoreComponent,
    FooterstoreComponent,
    StoreproductComponent,
    AddcarnewComponent,
    AgoPipe,
    StorehowtoComponent
  ],
  imports: [
    BrowserModule,
    CKEditorModule,
    NgxPaginationModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [Api],
  bootstrap: [AppComponent]
})
export class AppModule { }
