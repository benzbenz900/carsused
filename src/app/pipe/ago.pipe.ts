import { Pipe, PipeTransform } from '@angular/core';
import * as timeago from 'timeago.js';
@Pipe({
  name: 'ago'
})
export class AgoPipe implements PipeTransform {

  transform(value: any) {
    var locale = function(number, index, totalSec):any {
      return [
        ['เมือสักครู่', 'right now'],
        ['%s วิที่แล้ว', 'in %s seconds'],
        ['1 นาทีที่แล้ว', 'in 1 minute'],
        ['%s นาทีที่แล้ว', 'in %s minutes'],
        ['1 ชั่วโมงที่แล้ว', 'in 1 hour'],
        ['%s ชั่วโมงที่แล้ว', 'in %s hours'],
        ['1 วันที่แล้ว', 'in 1 day'],
        ['%s วันที่แล้ว', 'in %s days'],
        ['1 สัปดาห์ที่แล้ว', 'in 1 week'],
        ['%s สัปดาห์ที่แล้ว', 'in %s weeks'],
        ['1 เดือนที่แล้ว', 'in 1 month'],
        ['%s เดือนที่แล้ว', 'in %s months'],
        ['1 ปีที่แล้ว', 'in 1 year'],
        ['%s ปีที่แล้ว', 'in %s years']
      ][index];
    };
    timeago.register('th_TH', locale);

    return timeago.format(value, 'th_TH');;
  }

}
