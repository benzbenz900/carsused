export class Api {
    public BASE_API: string;
    public SERVER_URL: string;
    constructor() {
        this.BASE_API = 'http://localhost:3000/api/';
        this.SERVER_URL = "https://api.imgbb.com/1/upload?expiration=15552000&key=38f3274e21befc4877826853123eab34";
    }

    ValidateEmail(mail: any) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return true
        }
        return false
    }

    async getCarList(status: string = '', urlstore: string = '', pageNumber: number = 1, nPerPage: number = 2) {
        var skip = pageNumber > 0 ? ((pageNumber - 1) * nPerPage) : 0
        var sort = `&sort=desc&skip=${skip}&limit=${nPerPage}`
        var project = `project=imgURL.img_0.thumb.url,name,price,commission,province,datePost,urlStore,namestore,namesale,status${sort}`
        var q = '?' + project;
        if (status == '' && urlstore != '') {
            var q = `?q={"urlStore":"${urlstore}"}&${project}`;
        } else if (status != '' && urlstore == '') {
            var q = `?q={"status":"${status}"}&${project}`;
        } else if (status != '' && urlstore != '') {
            var q = `?q={"status":"${status}","urlStore":"${urlstore}"}&${project}`;
        }
        return new Promise(rest => {
            fetch(`${this.BASE_API}storecar/${q}`, { cache: "no-store" })
                .then(response => response.json())
                .then(result => {
                    rest(result)
                    console.log(result,'getStoredata')
                })
        })
    }

    checkprice(text: string) {
        var decimal = /^[-+]?[0-9]+$/;
        return decimal.test(text)
    }

    checkEng(text: string) {
        var letters = /^[A-Za-z]+$/;
        return letters.test(text)
    }

    chechPassword(text: string) {
        var passcheck = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,32}$/;
        return passcheck.test(text)
    }

    async uploadImage(fileInput: any) {
        return new Promise(rest => {
            var formdata = new FormData();
            formdata.append("image", fileInput[0]);

            var requestOptions = {
                method: 'POST',
                body: formdata
            };

            fetch(this.SERVER_URL, requestOptions)
                .then(response => response.json())
                .then(result => rest(result))
        })
    }

    async storeLogin(username: string, password: string) {
        var project = 'project=urlStore,nameStore,phone,password,fName,lName,imgURL,howto,contact,line,facebook,googlemap'
        if (this.ValidateEmail(username)) {
            var q = `{"email":"${username}","password":"${password}"}&${project}`;
        } else {
            var q = `{"username":"${username}","password":"${password}"}&${project}`;
        }
        return new Promise(rest => {
            fetch(`${this.BASE_API}storeuser/?q=${q}`, { cache: "no-store" })
                .then(response => response.json())
                .then(result => rest(result))
        })
    }

    async checkStoreUser(username: any) {
        var project = 'project=username'
        return new Promise(rest => {
            fetch(`${this.BASE_API}storeuser/?q={"username":"${username}"}&${project}`, { cache: "no-store" })
                .then(response => response.json())
                .then(result => rest(result))
        })
    }

    async checkStoreEmail(email: any) {
        var project = 'project=email'
        return new Promise(rest => {
            fetch(`${this.BASE_API}storeuser/?q={"email":"${email}"}&${project}`, { cache: "no-store" })
                .then(response => response.json())
                .then(result => rest(result))
        })
    }

    async getStoredata(urlStore: any) {
        var project = 'project=urlStore,nameStore,phone,password,fName,lName,imgURL,howto,contact,line,facebook,googlemap'
        return new Promise(rest => {
            fetch(`${this.BASE_API}storeuser/?q={"urlStore":"${urlStore}"}&${project}`, { cache: "no-store" })
                .then(response => response.json())
                .then(result => {
                    rest(result)
                    console.log(result,'getStoredata')
                })
        })
    }

    async checkStoreUrl(urlStore: any) {
        var project = 'project=urlStore,nameStore'
        return new Promise(rest => {
            fetch(`${this.BASE_API}storeuser/?q={"urlStore":"${urlStore}"}&${project}`, { cache: "no-store" })
                .then(response => response.json())
                .then(result => rest(result))
        })
    }

    async saveNewCarPost(arr: {}) {
        return new Promise(rest => {
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

            fetch(`${this.BASE_API}storecar`, {
                method: 'PUT',
                headers: myHeaders,
                body: JSON.stringify(arr)
            })
                .then(response => response.json())
                .then(result => rest(result))
        })
    }

    async saveUpdateStore(arr: {}, where: string = '') {
        if (where && where != '' && where != 'null' && where != 'undefined') {
            return new Promise(rest => {
                var myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");

                fetch(`${this.BASE_API}storeuser/?q={"urlStore":"${where}"}`, {
                    method: 'POST',
                    headers: myHeaders,
                    body: JSON.stringify(arr)
                })
                    .then(response => response.json())
                    .then(result => rest(result))
            })
        }
    }

    async registerStore(arr: {}) {
        return new Promise(rest => {
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

            fetch(`${this.BASE_API}storeuser`, {
                method: 'PUT',
                headers: myHeaders,
                body: JSON.stringify(arr)
            })
                .then(response => response.json())
                .then(result => rest(result))
        })
    }
}
