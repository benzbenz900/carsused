import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleregisterComponent } from './saleregister.component';

describe('SaleregisterComponent', () => {
  let component: SaleregisterComponent;
  let fixture: ComponentFixture<SaleregisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleregisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
