import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleloginComponent } from './salelogin.component';

describe('SaleloginComponent', () => {
  let component: SaleloginComponent;
  let fixture: ComponentFixture<SaleloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
