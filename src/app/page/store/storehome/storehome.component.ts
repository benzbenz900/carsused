import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Api } from 'src/app/service/api';
import { CKEditor4 } from 'ckeditor4-angular/ckeditor';
declare const $: any
@Component({
  selector: 'app-storehome',
  templateUrl: './storehome.component.html',
  styleUrls: ['./storehome.component.scss']
})
export class StorehomeComponent implements OnInit {
  public tokenUser = (localStorage.getItem('tokenIdUser')) ? JSON.parse(localStorage.getItem('tokenIdUser')) : false
  public json: {};
  constructor(private router: Router, private api: Api) { }

  ngOnInit(): void {
    if (!this.tokenUser) {
      this.router.navigate([`index.html`]);
    }

    this.json = {
      imgURL: {
        img_0: this.tokenUser.imgURL.img_0,
        img_1: this.tokenUser.imgURL.img_1,
        img_2: this.tokenUser.imgURL.img_2,
      },
      howto: this.tokenUser.howto,
      contact: this.tokenUser.contact,
      phone: this.tokenUser.phone,
      line: this.tokenUser.line,
      facebook: this.tokenUser.facebook,
      googlemap: this.tokenUser.googlemap,
      urlStore: this.tokenUser.urlStore,
      nameStore: this.tokenUser.nameStore,
    }
  }

  changeInput(name: string, value: string) {
    this.json[name] = value;
  }

  removePreview(item: string) {
    this.json['imgURL']['img_' + item] = false
  }

  changeInputckeditor(event: CKEditor4.EventInfo, name: string) {
    this.json[name] = event.editor.getData();
  }

  uploadImage(files: string | any[], item: any) {
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      alert('รองรับเฉพาะไฟล์รูปภาพเท่านั้น')
      return;
    }

    this.json['imgURL']['img_' + item] = './assets/loading.svg';
    this.api.uploadImage(files).then((res: any) => {
      if (res.success) {
        this.json['imgURL']['img_' + item] = res.data
      } else {
        this.json['imgURL']['img_' + item] = false
        alert("อัพโหลดรูปภาพไม่สำเร็จ")
      }
    })

  }

  saveDataToDatabase() {
    $('#loading').show()
    $('#form-info-data').hide()
    this.api.saveUpdateStore(this.json, this.tokenUser.urlStore).then((res) => {
      this.api.getStoredata(this.tokenUser.urlStore).then(res => {
        localStorage.setItem('tokenIdUser', JSON.stringify(res[0]))
        location.reload()
      })
    })
  }


}
