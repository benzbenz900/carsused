import { Component, OnInit } from '@angular/core';
import { Api } from 'src/app/service/api';
import { Md5 } from 'ts-md5/dist/md5';
import { Router } from '@angular/router';
declare const $: any
@Component({
  selector: 'app-storelogin',
  templateUrl: './storelogin.component.html',
  styleUrls: ['./storelogin.component.scss']
})
export class StoreloginComponent implements OnInit {
  public username: string;
  public json: {};

  constructor(public api: Api, private router: Router) { }

  ngOnInit(): void {
    this.json = {
      username: '',
      password: ''
    }
  }

  changeInput(name: string = '', event: { target: { value: any; }; }) {
    this.json[name] = event.target.value
  }

  showErrorText(text: string) {
    $('#show_error_text').html(text)
  }

  checkLogin() {
    if (this.json['username'] == '') {
      this.showErrorText('กรุณากรอก ชื่อผู้ใช้งาน หรือ อีเมล')
    } else if (this.json['password'] == '') {
      this.showErrorText('กรุณากรอกรหัสผ่าน')
    }
    $('#loading').show()
    $('.form-signin').hide()
    var password = Md5.hashStr(this.json['password']).toString();
    this.api.storeLogin(this.json['username'], password).then((res: any[]) => {
      if (res.length == 0) {
        this.showErrorText('ชื่อผู้ใช้งาน หรือ รหัสผ่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง')
      } else {
        localStorage.setItem('tokenIdUser', JSON.stringify(res[0]))
        this.showErrorText('')
        this.router.navigate([`store/${res[0].urlStore}/home`]);
      }
      $('#loading').hide()
      $('.form-signin').show()

    })
  }

}
