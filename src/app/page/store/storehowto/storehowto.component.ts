import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Api } from 'src/app/service/api';

@Component({
  selector: 'app-storehowto',
  templateUrl: './storehowto.component.html',
  styleUrls: ['./storehowto.component.scss']
})
export class StorehowtoComponent implements OnInit {
  public storeData: any = false;
  public islogin: boolean = false;
  public tokenUser: any = false;
  public pathname = window.location.pathname
  public data: { title: string; html: any; };
  constructor(private router: Router, private api: Api, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.tokenUser = (localStorage.getItem('tokenIdUser')) ? JSON.parse(localStorage.getItem('tokenIdUser')) : false
    if (this.tokenUser) {
      this.islogin = true
      this.storeData = this.tokenUser
    } else {
      this.route.params.subscribe(params => {
        if (params['storeurl']) {
          this.getStoreData(params['storeurl'])
        }
      });
    }

    if (this.pathname.indexOf('howto') != -1) {
      this.data = {
        title: 'วิธีการทำงาน',
        html: this.storeData['howto']
      }
    } else {
      this.data = {
        title: 'ติดต่อเรา',
        html: this.storeData['contact']
      }
    }
  }

  getStoreData(urlStore: any) {
    this.api.getStoredata(urlStore).then((res: any[]) => {
      if (res.length == 1) {
        this.storeData = res[0]
      } else {
        this.router.navigate([`index.html`]);
      }
    })
  }

}
