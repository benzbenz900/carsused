import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StorehowtoComponent } from './storehowto.component';

describe('StorehowtoComponent', () => {
  let component: StorehowtoComponent;
  let fixture: ComponentFixture<StorehowtoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StorehowtoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StorehowtoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
