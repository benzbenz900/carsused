import { Component, OnInit } from '@angular/core';
import { Api } from 'src/app/service/api';
import { ActivatedRoute } from '@angular/router';
import { PaginationInstance } from 'ngx-pagination/dist/ngx-pagination.module';
@Component({
  selector: 'app-storeproduct',
  templateUrl: './storeproduct.component.html',
  styleUrls: ['./storeproduct.component.scss']
})
export class StoreproductComponent implements OnInit {
  public carListData: any = 'loading'
  public pathname = window.location.pathname
  constructor(private api: Api, private route: ActivatedRoute) { }
  public config: PaginationInstance = {
    id: 'PaginationProduct',
    itemsPerPage: 8,
    currentPage: 1,
    totalItems: 0
  };

  ngOnInit(): void {
    if (this.pathname == '/') {
      this.pathname = 'index.html'
    } else if (this.pathname.indexOf('store/')) {
      this.pathname = './'
    }
    this.route.params.subscribe(params => {
      if (params['storeurl']) {
        if (params['page']) {
          this.config.currentPage = parseInt(params['page'])
        }
        this.getCarList(params['status'], params['storeurl'], this.config.currentPage, this.config.itemsPerPage)
      } else {
        if (params['page']) {
          this.config.currentPage = parseInt(params['page'])
        }
        this.getCarList(params['status'], '', this.config.currentPage, this.config.itemsPerPage)
      }
    });
  }




  getCarList(status: string, urlstore: string, page: number, npage: number) {
    if (status == 'book') {
      status = 'book';
    } else if (status == 'sale') {
      status = 'sale';
    } else if (status == 'open') {
      status = 'open';
    } else {
      status = ''
    }
    this.api.getCarList(status, urlstore, page, npage).then((res: any) => {
      this.carListData = res
      if (res.length > 0) {
        this.config.totalItems = res[0].q_item_total
      }
      
    })
  }

}
