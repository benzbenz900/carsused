import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcarnewComponent } from './addcarnew.component';

describe('AddcarnewComponent', () => {
  let component: AddcarnewComponent;
  let fixture: ComponentFixture<AddcarnewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcarnewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcarnewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
