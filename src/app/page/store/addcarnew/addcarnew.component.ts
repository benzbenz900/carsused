import { Component, OnInit } from '@angular/core';
import { Api } from 'src/app/service/api';
import { CKEditor4 } from 'ckeditor4-angular/ckeditor';
import { Router } from '@angular/router';
declare const $: any
@Component({
  selector: 'app-addcarnew',
  templateUrl: './addcarnew.component.html',
  styleUrls: ['./addcarnew.component.scss']
})
export class AddcarnewComponent implements OnInit {
  public imagePath: any;
  public message: string;
  public json: {};
  public tokenUser = (localStorage.getItem('tokenIdUser')) ? JSON.parse(localStorage.getItem('tokenIdUser')) : false
  constructor(private router: Router, private api: Api) { 
    
  }

  ngOnInit(): void {
    if(!this.tokenUser){
      this.router.navigate([`index.html`]);
    }
    this.json = {
      imgURL: {
        img_0: false,
        img_1: false,
        img_2: false,
        img_3: false,
        img_4: false,
        img_5: false,
        img_6: false,
        img_7: false,
        img_8: false,
        img_9: false,
        img_10: false,
        img_11: false,
        img_12: false,
        img_13: false,
        img_14: false,
        img_15: false,
        img_16: false,
        img_17: false
      },
      name: '',
      price: '',
      commission: '',
      province: '',
      description: '',
      datePost: '',
      urlStore: this.tokenUser.urlStore,
      namestore:this.tokenUser.nameStore,
      namesale:false,
      status:'open'
    }
  }

  changeInputckeditor(event: CKEditor4.EventInfo) {
    this.json['description'] = event.editor.getData();
  }

  removePreview(item: string) {
    this.json['imgURL']['img_' + item] = false
  }

  changeInput(name: string, value: string) {
    this.json[name] = value;
  }

  uploadImage(files: string | any[], item: any) {
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      alert('รองรับเฉพาะไฟล์รูปภาพเท่านั้น')
      return;
    }

    this.json['imgURL']['img_' + item] = './assets/loading.svg';
    this.api.uploadImage(files).then((res: any) => {
      if (res.success) {
        this.json['imgURL']['img_' + item] = res.data
      } else {
        this.json['imgURL']['img_' + item] = false
        alert("อัพโหลดรูปภาพไม่สำเร็จ")
      }
    })

  }

  saveDataToDatabase() {
    if (!this.json['imgURL']['img_0']) {
      alert('กรุณาเพิ่มรูป หน้าปก')
    } else if (this.json['name'] == '') {
      alert('กรุณาระบุชื่อประกาศ')
    } else if (this.json['price'] == '') {
      alert('กรุณาระบุราคารถ')
    } else if (this.json['commission'] == '') {
      alert('กรุณาระบุค่าคอมมิชชั่น')
    } else if (this.json['province'] == '') {
      alert('กรุณาระบุจังหวัด ที่เข้าชมรถได้')
    } else if (this.json['description'] == '') {
      alert('กรุณาระบุรายละเอียดของรถโดยประมาณ')
    } else {
      $('#loading').show()
      $('#form-info-data').hide()
      this.json['datePost'] = new Date().getTime();

      this.api.saveNewCarPost(this.json).then(() => {
        this.router.navigate([`store/${this.tokenUser.urlStore}/home`]);
      })
    }
  }

}
