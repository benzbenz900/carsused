import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StorehomepageComponent } from './storehomepage.component';

describe('StorehomepageComponent', () => {
  let component: StorehomepageComponent;
  let fixture: ComponentFixture<StorehomepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StorehomepageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StorehomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
