import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Api } from 'src/app/service/api';
declare const $: any
@Component({
  selector: 'app-storehomepage',
  templateUrl: './storehomepage.component.html',
  styleUrls: ['./storehomepage.component.scss']
})
export class StorehomepageComponent implements OnInit {
  public style_css: string = (localStorage.getItem('style_css')) ? localStorage.getItem('style_css') : 'pulse';
  public storeData: any = false;
  public islogin: boolean = false;
  public tokenUser: any = false;
  constructor(private router: Router, private api: Api, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.tokenUser = (localStorage.getItem('tokenIdUser')) ? JSON.parse(localStorage.getItem('tokenIdUser')) : false
    if (this.tokenUser) {
      this.islogin = true
      this.storeData = this.tokenUser
    } else {
      this.route.firstChild.params.subscribe(params => {
        if (params['storeurl']) {
          this.getStoreData(params['storeurl'])
        }
      });
    }
    this.changeStyle()
  }

  getStoreData(urlStore: any) {
    this.api.getStoredata(urlStore).then((res: any[]) => {
      console.log(res)
      if (res.length == 1) {
        this.storeData = res[0]
      } else {
        this.router.navigate([`index.html`]);
      }
    })
  }

  
  changeStyle() {
    if (this.style_css != 'darkly') {
      $('#style_css').attr('href', `https://bootswatch.com/4/flatly/bootstrap.min.css`);
      this.style_css = 'darkly';
      localStorage.setItem('style_css', 'pulse')
    } else {
      $('#style_css').attr('href', `https://bootswatch.com/4/darkly/bootstrap.min.css`);
      this.style_css = 'pulse';
      localStorage.setItem('style_css', 'darkly')
    }
  }

}
