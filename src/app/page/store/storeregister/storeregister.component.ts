import { Component, OnInit } from '@angular/core';
import { Api } from 'src/app/service/api';
import { Router } from '@angular/router';
import { Md5 } from 'ts-md5/dist/md5';
declare const $: any
@Component({
  selector: 'app-storeregister',
  templateUrl: './storeregister.component.html',
  styleUrls: ['./storeregister.component.scss']
})
export class StoreregisterComponent implements OnInit {
  public json: {};
  public step_number: string = '1';
  public domain: string;
  public randomString: string = Math.random().toString(36).substring(7);;
  constructor(public api: Api, private router: Router) { }

  ngOnInit(): void {
    this.json = {
      nameStore: '',
      typeStore: '',
      urlStore: '',
      fName: '',
      lName: '',
      username: '',
      password: '',
      password_confirm: '',
      email: '',
      phone: '',
      imgURL: {
        img_0: false,
        img_1: false,
        img_2: false,
      },
      howto: '',
      contact: '',
      line: '',
      facebook: '',
      googlemap: '',
    }

    this.domain = `${window.location.protocol}//${window.location.hostname}/store/`;

  }

  saveDataStore() {
    this.api.registerStore(this.json)
      .then((res) => {
        this.showErrorText('สมัครสมาชิกร้าน สำเร็จ คุณสามารถ เข้าสู่ระบบ เพื่อลงขายได้ทันที')
        this.router.navigate(['storelogin.html']);
      })
  }

  errorShow(element: any) {
    $(`[name=${element}]`).removeClass('is-valid')
    $(`[name=${element}]`).addClass('is-invalid')
  }

  passShow(element: any) {
    $(`[name=${element}]`).addClass('is-valid')
    $(`[name=${element}]`).removeClass('is-invalid')
  }

  changeStep(step_pev: string = '', step_next: string = '', step_number: string = '1', ...check: any[]) {
    var passCheck = true;

    if (typeof check == 'object') {
      check.forEach((element: any) => {
        if (this.json[element] == '') {
          this.errorShow(element)
          passCheck = false
        } else {
          this.passShow(element)
        }
      });

      if (passCheck) {
        this.step_number = step_number
        if (step_pev != '') {
          $(`#${step_pev}`).fadeOut(200, function () {
            $(`#${step_pev}`).hide()
            $(`#${step_next}`).show()
          });
        }

        if (step_next == 'step_5') {
          this.saveDataStore()
        }
      }
    }
  }

  showErrorText(text: string) {
    $('#show_error_text').html(text)
  }

  changeInputRadio(name: string = '', value: string = '') {
    this.json[name] = value
  }

  checkInput(name: string = '', step_btn: string) {
    if (name == 'email') {
      if (this.api.ValidateEmail(this.json[name])) {
        this.api.checkStoreEmail(this.json[name]).then((res: []) => {
          if (res.length > 0) {
            this.showErrorText(`มีท่านอื่นใช้ ${this.json[name]} ไปแล้ว`)
            this.errorShow(name)
            $(`.${step_btn}`).hide()
          } else {
            this.passShow(name)
            $(`.${step_btn}`).show()
          }
        })
      } else {
        this.showErrorText("รูปแบบ อีเมลไม่ถูกต้อง")
        this.errorShow(name)
        $(`.${step_btn}`).hide()
      }
    } else if (name == 'password') {
      if (this.api.chechPassword(this.json[name])) {
        this.passShow(name)
        $(`.${step_btn}`).show()
      } else {
        this.showErrorText(`-ต้องมีทั้งตัวเลขและตัวอักษร\n
        -ต้องมีทั้งตัวอักษรพิมพ์เล็กและพิมพ์ใหญ่และมีตัวเลขอย่างน้อย 1 ตัว\n
        -ต้องมีความยาวอย่างน้อย 8 ตัวแต่ไม่เกิน 32 ตัว`)
        this.errorShow(name)
        $(`.${step_btn}`).hide()
      }
    } else if (name == 'password_confirm') {
      if (this.json[name] == this.json['password']) {
        this.json[name] = this.json['password'] = Md5.hashStr(this.json[name]).toString()
        this.passShow(name)
        $(`.${step_btn}`).show()
      } else {
        this.showErrorText(`รหัสผ่านที่คุณตั้ง ไม่ตรงกัน`)
        this.errorShow(name)
        $(`.${step_btn}`).hide()
      }
    } else if (name == 'username') {
      this.api.checkStoreUser(this.json[name]).then((res: []) => {
        if (res.length > 0) {
          this.showErrorText(`มีท่านอื่นใช้ ${this.json[name]} ไปแล้ว`)
          this.errorShow(name)
          $(`.${step_btn}`).hide()
        } else {
          this.passShow(name)
          $(`.${step_btn}`).show()
        }
      })
    } else if (name == 'urlStore') {
      if (this.json[name].length >= 6) {
        if (this.api.checkEng(this.json[name])) {
          this.api.checkStoreUrl(this.json[name]).then((res: []) => {
            if (res.length > 0) {
              this.showErrorText(`มีท่านอื่นใช้ ${this.domain}${this.json[name]} ไปแล้ว`)
              this.errorShow(name)
              $(`.${step_btn}`).hide()
            } else {
              this.passShow(name)
              $(`.${step_btn}`).show()
            }
          })
        } else {
          this.showErrorText(`URL ร้านต้องเป็นภาษาอังกฤษ เท่านั้น และห้ามเว้นวรรค์`)
          this.errorShow(name)
          $(`.${step_btn}`).hide()
        }
      } else {
        this.showErrorText(`ความยาวของ URL ร้าน ต้องมีอย่างน้อย 6 ตัวอักษร`)
        this.errorShow(name)
        $(`.${step_btn}`).hide()
      }
    } else {
      this.passShow(name)
      $(`.${step_btn}`).show()
    }
  }

  changeInput(name: string = '', event: { target: { value: any; }; }) {
    this.json[name] = event.target.value
  }

}
