import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreregisterComponent } from './storeregister.component';

describe('StoreregisterComponent', () => {
  let component: StoreregisterComponent;
  let fixture: ComponentFixture<StoreregisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreregisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
