import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare const $: any
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public style_css: string = (localStorage.getItem('style_css')) ? localStorage.getItem('style_css'):'pulse';
  public storeData: any = false;

  constructor(private router: Router) { }

  ngOnInit(): void {
    document.body.style.backgroundImage = 'none'
    this.storeData = (localStorage.getItem('tokenIdUser')) ? JSON.parse(localStorage.getItem('tokenIdUser')):false
    this.changeStyle()
  }
  logOut(){
    localStorage.removeItem('tokenIdUser')
    this.storeData = (localStorage.getItem('tokenIdUser')) ? JSON.parse(localStorage.getItem('tokenIdUser')):false
    this.router.navigate([`index.html`]);
  }
  changeStyle(){
    if (this.style_css != 'darkly') {
      $('#style_css').attr('href', `https://bootswatch.com/4/flatly/bootstrap.min.css`);
      this.style_css = 'darkly';
      localStorage.setItem('style_css', 'pulse')
    } else {
      $('#style_css').attr('href', `https://bootswatch.com/4/darkly/bootstrap.min.css`);
      this.style_css = 'pulse';
      localStorage.setItem('style_css', 'darkly')
    }
  }

}
