import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterstoreComponent } from './footerstore.component';

describe('FooterstoreComponent', () => {
  let component: FooterstoreComponent;
  let fixture: ComponentFixture<FooterstoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterstoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterstoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
