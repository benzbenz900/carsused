import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Api } from 'src/app/service/api';

@Component({
  selector: 'app-headerstore',
  templateUrl: './headerstore.component.html',
  styleUrls: ['./headerstore.component.scss']
})
export class HeaderstoreComponent implements OnInit {
  @Input() style_css: any;
  @Input() storeData: any;
  @Input() islogin: any;
  @Input() tokenUser: any;
  @Input() changeStyle: any
  constructor(private router: Router, private api: Api) { }

  ngOnInit(): void {
    if (this.storeData) {
      document.body.style.backgroundImage = `url(${this.storeData.imgURL.img_2.url})`;
    }
  }

  checkRouter(r:string){
    return this.router.url.includes(r)
  }

  logOut() {
    localStorage.removeItem('tokenIdUser')
    this.router.navigate([`index.html`]);
  }

}
